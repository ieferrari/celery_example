from fastapi import FastAPI, BackgroundTasks
from time import sleep
from celery_tasks import *

app=FastAPI()

def long_excecution_time_func():
    sleep(10)
    print('work is done!')

@app.get('/')
async def index():
    long_excecution_time_func()
    return {'result':'success'}

@app.get('/background')
async def back(background_tasks: BackgroundTasks):
    background_tasks.add_task(long_excecution_time_func)
    return{'result':'success'}


@app.get('/msg/{msg}')
async def celery_call(msg: str):
    my_function_call = long_excecution_with_celery(msg)
    return {
        'task_id': str(my_function_call),
        'status':'in progress'
        }



from db import *

@app.get('/get/{task_id}')
async def get_task_status(task_id: str):
    session = SessionLocal()
    try:
        status = session\
                    .query(Celerytask_meta)\
                    .filter_by(task_id=str(task_id))\
                    .first()\
                    .status
        return {
            'task_id': task_id,
            'status':status
        }
    except AttributeError:
        return {
            'task_id': task_id,
            'status':"not found, task_id does not exists or is in progress"
        }