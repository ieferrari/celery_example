from celery import Celery
from time import sleep


# check this tutorial https://www.youtube.com/watch?v=THxCy-6EnQM
app = Celery(
    'tasks',
    broker='amqp://localhost:5672',    # docker-compose up to run the broker
    backend='db+sqlite:///db.sqlite3'  # local db to track done tasks
             )


@app.task
def long_excecution_time_func(my_msg):
    sleep(2)
    # TO_DO write in progress to db
    sleep(3)
    file = my_msg+'.txt'
    with open(file, 'wb') as f:
        f.write(bytes(my_msg, 'utf-8'))


def long_excecution_with_celery(my_msg):
    '''
    This function will pass the task to a cellery worker, 
    So, you need to have cellery up and running in another terminal, with:        
        celery -A celery_tasks worker --loglevel=info
    '''
    my_function_call = long_excecution_time_func.delay(my_msg)
    long_excecution_time_func.delay(my_msg)
    print("in progress")
    return my_function_call
