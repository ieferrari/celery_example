# Celery example
When you want to run a task that takes too long, then:

* pass the message to the broker (rabbitmq)
* message broker tells celery to do the task
* celery spawns a worker to to the task


***
## Prerequisites:

### Create your python virtual env for this project
open a terminal in the root folder of the repo and run:

    python -m venv env

activate your new environment:

    source ./env/bin/activate
    # the path is different in windows..

Install requirements.txt:

    pip instal -r requirements.txt

### Activate message broker:
You activate and run rabbitmq broker on port 5672 by opening a terminal and running:
    
    docker-compose up

### Create an instance of Celery
To create a celery instance in charge getting the tasks done, we need to open another terminal and run:

    celery -A celery_tasks worker --loglevel=info

***

## Run celery examples


Check the [notebook](/notebook1.ipynb)