from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


SQLALCHEMY_DATABASE_URL = "sqlite:///db.sqlite3"
engine = create_engine(SQLALCHEMY_DATABASE_URL,
                       connect_args={"check_same_thread": False}
                       )


SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# database Models #######################################################

Base = declarative_base()

class Celerytask_meta(Base):
    __tablename__ = 'celery_taskmeta'
    id = Column(Integer, primary_key=True)
    task_id = Column(String)
    status = Column(String)
    result = Column(String)
    date_done = Column(Date)

 

#to run in python shell:  exec(open('db.py').read())